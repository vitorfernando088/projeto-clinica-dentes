const express = require('express');
const sequelize = require('./database');

const app = express();
const port = 3000;

app.use(express.json());

// Rota para agendar uma consulta
app.post('/agendar', async (req, res) => {
  const { especialidade, data, horario } = req.body;

  try {
    const consulta = await Consulta.create({
      especialidade,
      data,
      horario
    });

    console.log('Consulta agendada com sucesso:', consulta.toJSON());
    res.send('Consulta agendada com sucesso.');
  } catch (err) {
    console.error('Erro ao agendar consulta:', err);
    res.status(500).send('Erro ao agendar consulta.');
  }
});

// Rota para exibir as consultas agendadas
app.get('/consultas', async (req, res) => {
  try {
    const consultas = await Consulta.findAll();

    res.send(consultas);
  } catch (err) {
    console.error('Erro ao buscar consultas agendadas:', err);
    res.status(500).send('Erro ao buscar consultas agendadas.');
  }
});

app.listen(port, () => {
  console.log(`Servidor rodando em http://localhost:${port}`);
});
