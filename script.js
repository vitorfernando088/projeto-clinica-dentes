// Array para armazenar as especialidades médicas disponíveis
let especialidades = [];

// Array para armazenar os horários disponíveis para agendamento
let horarios = [];

// Objeto para armazenar os agendamentos
const agendamentos = {};


// Função para adicionar um horário
function adicionarHorario() {
  const horario = prompt('Digite o horário disponível:');
  horarios.push(horario);
  alert(`Horário "${horario}" adicionado com sucesso!`);
}

// Função para agendar uma consulta
function agendarConsulta() {
  const especialidadeSelecionada = document.querySelector('input[name="especialidade"]:checked');
  if (!especialidadeSelecionada) {
    alert('Selecione uma especialidade!');
    return;
  }

  const especialidade = especialidadeSelecionada.value;

  const horarioSelecionado = document.querySelector('input[name="horario"]:checked');
  if (!horarioSelecionado) {
    alert('Selecione um horário!');
    return;
  }

  const horario = horarioSelecionado.value;

  if (!agendamentos[especialidade]) {
    agendamentos[especialidade] = [];
  }

  agendamentos[especialidade].push(horario);

  alert('Consulta agendada com sucesso!');
  limparSelecoes();
  exibirAgendamentos();
}

// Função para exibir a tela de Agendar Consulta
function exibirAgendarConsulta() {
  ocultarTelas();
  document.getElementById('agendarConsulta').style.display = 'block';
}

// Função para exibir a tela de Exibir Agendamentos
function exibirAgendamentos() {
  ocultarTelas();
  document.getElementById('exibirAgendamentos').style.display = 'block';

  let agendamentosHTML = '<h3>Agendamentos realizados:</h3>';
  for (const especialidade in agendamentos) {
    agendamentosHTML += `<p>${especialidade}: ${agendamentos[especialidade].join(', ')}</p>`;
  }
  document.getElementById('agendamentos').innerHTML = agendamentosHTML;
}

// Função para exibir as especialidades disponíveis como botões de opção
function exibirEspecialidades() {
  ocultarTelas();
  document.getElementById('selecionarEspecialidade').style.display = 'block';

  const especialidadesDiv = document.getElementById('especialidades');
  especialidadesDiv.innerHTML = '';

  for (let i = 0; i < especialidades.length; i++) {
    const especialidade = especialidades[i];
    const radioBtn = document.createElement('input');
    radioBtn.type = 'radio';
    radioBtn.name = 'especialidade';
    radioBtn.value = especialidade;

    const label = document.createElement('label');
    label.textContent = especialidade;

    const div = document.createElement('div');
    div.appendChild(radioBtn);
    div.appendChild(label);

    especialidadesDiv.appendChild(div);
  }
}

// Função para exibir os horários disponíveis como botões de opção
function exibirHorarios() {
  ocultarTelas();
  document.getElementById('selecionarHorario').style.display = 'block';

  const horariosDiv = document.getElementById('horarios');
  horariosDiv.innerHTML = '';

  for (let i = 0; i < horarios.length; i++) {
    const horario = horarios[i];
    const radioBtn = document.createElement('input');
    radioBtn.type = 'radio';
    radioBtn.name = 'horario';
    radioBtn.value = horario;

    const label = document.createElement('label');
    label.textContent = horario;

    const div = document.createElement('div');
    div.appendChild(radioBtn);
    div.appendChild(label);

    horariosDiv.appendChild(div);
  }
}

// Função para exibir a tela de Confirmação de Agendamento
function exibirConfirmacaoAgendamento() {
  ocultarTelas();
  document.getElementById('confirmarAgendamento').style.display = 'block';

  const especialidadeSelecionada = document.querySelector('input[name="especialidade"]:checked');
  document.getElementById('confirmEspecialidade').textContent = especialidadeSelecionada.value;

  const horarioSelecionado = document.querySelector('input[name="horario"]:checked');
  document.getElementById('confirmHorario').textContent = horarioSelecionado.value;
}

// Função para limpar as seleções feitas pelo usuário
function limparSelecoes() {
  const especialidadeButtons = document.querySelectorAll('input[name="especialidade"]');
  especialidadeButtons.forEach(button => {
    button.checked = false;
  });

  const horarioButtons = document.querySelectorAll('input[name="horario"]');
  horarioButtons.forEach(button => {
    button.checked = false;
  });
}

// Função para ocultar todas as telas
function ocultarTelas() {
  const telas = document.getElementsByClassName('tela');
  for (let i = 0; i < telas.length; i++) {
    telas[i].style.display = 'none';
  }
}

// Inicialização
especialidades = ['Cirurgia Odontologia Geral', 'Endodontia', 'Odontologia' , 'Ortodontia', 'Periodontia Geral', 'Protese Geral'];
horarios = ['08:00', '09:00', '10:00', '11:00', '14:00', '15:00', '16:00'];

exibirAgendarConsulta();
